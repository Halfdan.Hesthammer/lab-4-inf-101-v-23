package no.uib.inf101.colorgrid;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.TooManyListenersException;

public class ColorGrid implements IColorGrid{
  private ArrayList<CellColor> grid;
  private final int rows;
  private final int columns;

  /**
   * Takes the rows and columns and initializees the grid with new CellColor with color null.
   * @param rows
   * @param columns
   */
  public ColorGrid(int rows, int columns){
    this.grid = new ArrayList<CellColor>();
    this.rows = rows;
    this.columns = columns;
    for(int r = 0; r < rows; r++){
      for(int c = 0; c < columns; c++){
        grid.add(new CellColor(new CellPosition(r, c), null));
        int pos = r*columns + c;
        

      }
    }
  }
  
  //Returns amount of rows
  @Override
  public int rows() {
    // TODO Auto-generated method stub
    return rows;
  }
  //Returns amount of columns
  @Override
  public int cols() {
    // TODO Auto-generated method stub
    return columns;
  }
  //Returns the grid as a List containong CellColor objects
  @Override
  public List<CellColor> getCells() {
    // TODO Auto-generated method stub
    return grid;
  }

  // Returns the item at a CellPosition which contains a row and a column. 
  // Use the formula row*columns + column to get the 2 dimensional position
  // of a 1 dimensional list
  @Override
  public Color get(CellPosition pos) {
    // TODO Auto-generated method stub
    if(pos.row() >= rows || pos.row() < 0){
      throw new IndexOutOfBoundsException("Index out of bounds");
     }
     if(pos.col() >= columns || pos.col() < 0){
      throw new IndexOutOfBoundsException("Index out of bounds");
     }
    return grid.get(pos.row()*columns + pos.col()).color();
      
    
    
  }
  // Sets the color of a position
  @Override
  public void set(CellPosition pos, Color color) {
   if(pos.row() >= rows || pos.row() < 0){
    throw new IndexOutOfBoundsException("Index out of bounds");
   }
   if(pos.col() >= columns || pos.col() < 0){
    throw new IndexOutOfBoundsException("Index out of bounds");
   }
    grid.set(pos.row()*columns + pos.col(), new CellColor(pos, color));
      
    
    // TODO Auto-generated method stub
    
    
  }
  // TODO: Implement this class
}
