package no.uib.inf101.gridview;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Rectangle;
import java.awt.Color;
import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

public class GridView extends JPanel{
  // TODO: Implement this class
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
  IColorGrid grid;
  public GridView(IColorGrid grid){
    this.setPreferredSize(new Dimension(400, 300));
    this.grid = grid;
  }
  // Draws a grid
  @Override
  public void paintComponent(Graphics g) {
      // TODO Auto-generated method stub
      super.paintComponent(g);
      Graphics2D g2 = (Graphics2D) g;
      
      drawGrid(g2);
  }

  /**
   *  Draws a grid on the @param g2 Graphics2D object.
   * @param g2
   */
  public void drawGrid(Graphics2D g2){
    
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;
    Shape rec = new Rectangle.Double(OUTERMARGIN, OUTERMARGIN, width, height);
    g2.setColor(MARGINCOLOR);
    g2.fill(rec);
    
    CellPositionToPixelConverter cptpc = new CellPositionToPixelConverter(new Rectangle.Double(OUTERMARGIN, OUTERMARGIN, width, height), this.grid, OUTERMARGIN);
    drawCells(g2, grid, cptpc);
  }

  private static void drawCells(Graphics2D g2, CellColorCollection grid2, CellPositionToPixelConverter cptpc) {
    for (CellColor cell : grid2.getCells()) {
      if(cell.color() == null){
        g2.setColor(Color.DARK_GRAY);
        g2.fill(cptpc.getBoundsForCell(new CellPosition(cell.cellPosition().row(), cell.cellPosition().col())));
      }
      else{
        g2.setColor(cell.color());
        g2.fill(cptpc.getBoundsForCell(new CellPosition(cell.cellPosition().row(), cell.cellPosition().col())));

      }
      
    }
  }
  
}
