package no.uib.inf101.gridview;

import java.awt.Color;

import javax.swing.JFrame;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;

public class Main {
  public static void main(String[] args) {
    // TODO: Implement this method
    IColorGrid grid = new ColorGrid(3, 4);
    
    GridView view = new GridView(grid);
    JFrame frame = new JFrame();
    frame.setContentPane(view);
    frame.setTitle("Frame");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }
}
