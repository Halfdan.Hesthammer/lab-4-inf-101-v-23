package no.uib.inf101.bonus;

import java.awt.Dimension;

import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {

    // Kopier inn main-metoden fra kursnotatene om grafikk her,
    // men tilpass den slik at du oppretter et BeautifulPicture -objekt
    // som lerret.
    BeautifulPicture pic = new BeautifulPicture();
    JFrame frame = new JFrame();
    frame.setSize(new Dimension(500, 500));
    frame.setContentPane(pic);
    frame.setTitle("Sunset over Farm - Halfdan");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);

  }
}
