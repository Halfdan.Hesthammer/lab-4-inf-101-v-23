package no.uib.inf101.bonus;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Polygon;

import javax.swing.JPanel;

public class BeautifulPicture extends JPanel {

  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    this.setSize(new Dimension(getWidth(), getHeight()));

    drawSky(g);
    drawSun(g);
    drawGround(g);
    drawHouse(g);
    drawTree(g);

  }

  private void drawTree(Graphics g) {
    g.setColor(new Color(85, 43, 0));
    g.fillRect(4 * getWidth() / 5, (int) (getHeight() * 3.8 / 5), getWidth() / 13, getHeight() / 4);
    g.setColor(Color.ORANGE);
    Polygon pol = new Polygon();
    pol.addPoint(4 * getWidth() / 5 - getWidth() / 20, (int) (4.5 * getHeight() / 5));
    pol.addPoint(4 * getWidth() / 5 + getWidth() / 20 + getWidth() / 13, (int) (4.5 * getHeight() / 5));
    pol.addPoint(4 * getWidth() / 5 + (getWidth() / 20) / 2, (int) (3 * getHeight() / 5));
    g.setColor(new Color(200, 200, 0));
    g.fillPolygon(pol);

  }

  private void drawHouse(Graphics g) {
    g.setColor(Color.RED);
    g.fillRect(getWidth() / 6, (int) (4.2 * getHeight() / 5), getWidth() / 5, getHeight() / 8);
    Polygon pol = new Polygon();
    pol.addPoint(getWidth() / 6, (int) (4.2 * getHeight() / 5));
    pol.addPoint(getWidth() / 6 + getWidth() / 5, (int) (4.2 * getHeight() / 5));
    pol.addPoint(getWidth() / 6 + (getWidth() / 5 / 2), (int) (3.8 * getHeight() / 5));
    g.setColor(Color.black);
    g.fillPolygon(pol);
  }

  private void drawGround(Graphics g) {
    g.setColor(new Color(0, 128, 0));
    g.fillRect(0, getHeight() - getHeight() / 5, getWidth(), getHeight() / 5);
  }

  private void drawSun(Graphics g) {
    int sunRadiusW = getWidth() / 10;
    int sunRadiusH = getHeight() / 10;
    int sunX = getWidth() / 2 - sunRadiusW;
    float sunY = (float) (3.3 * getHeight() / 5);
    g.setColor(Color.ORANGE);
    g.fillOval(sunX, (int) sunY, sunRadiusW * 2, sunRadiusH * 2);

  }

  private void drawSky(Graphics g) {
    setBackground(Color.WHITE);

    Color topColor = new Color(0, 102, 204);
    Color bottomColor = new Color(255, 153, 51);
    GradientPaint gradient = new GradientPaint(0, 0, topColor, 0, getHeight() - getHeight() / 5, bottomColor);
    Graphics2D g2d = (Graphics2D) g;
    g2d.setPaint(gradient);
    g2d.fillRect(0, 0, getWidth(), getHeight());

  }

}
